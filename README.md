# About

Snippy is a simple DIY text expander for Linux originally described in http://www.linux-magazine.com/Issues/2014/162/Workspace-Text-Expander

# Dependencies

- xdotool
- xsel 

On openSUSE, run the following command to install the required packages:

    sudo zypper in xdotool xsel

